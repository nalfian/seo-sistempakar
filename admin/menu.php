<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <!-- search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
		  <a href="..">
            <li class="header">Lihat Website : SEO On Page</li>
			</a>

      <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-duplicate"></i> <span>Pertanyaan</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?page=form_pertanyaan"><i class="glyphicon glyphicon-pencil active"></i>Form pertanyaan</a></li>
                <li><a href="index.php?page=list_pertanyaan"><i class="glyphicon glyphicon-list active"></i>List Pertanyaan</a></li>
              </ul>
            </li> 

       <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-bullhorn"></i><span>History</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?page=list_konsultasi"><i class="glyphicon glyphicon-list active"></i>List Konsultasi</a></li>
              </ul>
            </li>

      <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-object-align-vertical"></i><span>Admin</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="index.php?page=form_admin"><i class="glyphicon glyphicon-pencil"></i>Form Admin</a></li>
                <li><a href="index.php?page=list_admin"><i class="glyphicon glyphicon-list active"></i>List Admin</a></li>
              </ul>
            </li>

			<li><a href="logout.php"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
</aside>